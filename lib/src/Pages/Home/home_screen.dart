import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:websiet_chapitec/src/Pages/Home/Components/presentation.dart';
import 'package:websiet_chapitec/src/Pages/Home/Components/products.dart';
import 'package:websiet_chapitec/src/Pages/Home/Components/team.dart';
import 'package:websiet_chapitec/src/Styles/styles.dart';
import 'package:url_launcher/url_launcher.dart';  

class HomeScreen extends StatefulWidget {
  static const String name = "HomeScreen";
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {


  void openWhatsAppChat() async {
    String url ="https://wa.me/51925576092/?text=Hola ChapItec quisiera mas informacion sobre los servicio que oferecen para mi empresa";
    // ignore: deprecated_member_use
    if (await canLaunch(url)) {
      // ignore: deprecated_member_use
      await launch(url);
    } else {
      throw 'No se pudo abrir WhatsApp.';
    }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 0, 0, 0),
        toolbarHeight: 85,
        actions: itemsNavbar(),
        title: iconLogo(),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          color: const Color.fromARGB(255, 16, 37, 80),
          child: Column(
            children: [
              Stack(
                children: [
                  SizedBox(
                    height: size.height * 0.8,
                    width: double.infinity,
                    child: Transform.scale(
                      scale: 1.9,
                      child: SvgPicture.asset(
                        'assets/images/fondo.svg',
                      ),
                    ),
                  ),
                  Container(
                    height: size.height * 0.8,
                    width: double.infinity,
                    decoration: const BoxDecoration(
                      color: Color.fromARGB(132, 4, 5, 8),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Revoluciona tu Negocio',
                            style:
                                titlelora(fontSize: 110, color: Colors.white)),
                        const SizedBox(height: 20),
                        const Text(
                            'Paginas web y Apps Móviles empresariales de elite para llevar tu empresa al siguiente nivel ',
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: Colors.white, fontSize: 30)),
                        const Text('! Has que tu presencia digital destaque !',
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: Colors.white, fontSize: 30)),
                        const SizedBox(
                          height: 60,
                        ),
                        MaterialButton(
                          onPressed: () {openWhatsAppChat();},
                          color: Colors.blue.shade900,
                          padding: const EdgeInsets.all(25),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          minWidth: 250,
                          child: const Text('Contactanos',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 24)),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Container(
                width: size.width * 0.95,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: const Column(
                  children: [
                    ProductAdvantages(),
                    // Container(
                    //   color: Colors.black,
                    //   height: 15,
                    //   width: double.infinity,
                    // ),
                    ProductsComponent(),
                    TeamGroup()
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  
  }

  List<Widget> itemsNavbar() {
    return [
      Text(
        'ABOUT',
        style: white_text,
      ),
      const SizedBox(width: 30),
      Text(
        'REVIEWS',
        style: white_text,
      ),
      const SizedBox(width: 30),
      Text(
        'WHY US',
        style: white_text,
      ),
      const SizedBox(width: 30),
      Text(
        'CONTACT',
        style: white_text,
      ),
      const SizedBox(width: 200)
    ];
  }

  Widget iconLogo() {
    return Row(
      children: [
        const SizedBox(
          width: 200,
        ),
        Image.asset(
          'assets/logo/logo.png',
          scale: 1.1,
        ),
      ],
    );
  }
}
