import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:websiet_chapitec/src/Styles/styles.dart';

class ProductAdvantages extends StatelessWidget {
  const ProductAdvantages({super.key});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.95,
      padding: const EdgeInsets.all(80),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            '¿Porque nuestro producto es el mejor?',
            style: GoogleFonts.lora(
              color: Colors.grey.shade700,
              fontSize: 35,
              fontWeight: FontWeight.w700,
            ),
          ),
          const SizedBox(height: 45),
          SizedBox(
            width: size.width * 0.5,
            child: Text(
              'Nuestro producto sobresale gracias a su arquitectura avanzada, características vanguardistas y una interfaz intuitiva. Nos esforzamos continuamente por superar las expectativas y proporcionar soluciones tecnológicas que marquen la diferencia.',
              textAlign: TextAlign.center,
              style: containtextlora(fontSize: 20),
              
            ),
          ),
          const SizedBox(height: 35),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: itempresentation(
                  description:
                      'Diseñado con responsividad en mente, nuestro software garantiza una experiencia fluida en todos los dispositivos, desde computadoras de escritorio hasta dispositivos móviles. Ofrecemos la flexibilidad que tu negocio necesita para prosperar en cualquier entorno digital.',
                  service: 'Responsive',
                  urlImage: 'assets/images/responsive.svg'
                ),
              ),
              Expanded(
                child: itempresentation(
                  description:
                      'Nuestra dedicación a la calidad se refleja en cada línea de código. Cada función, cada actualización, se somete a rigurosas pruebas para garantizar un rendimiento óptimo, seguridad sólida y una experiencia sin igual para nuestros usuarios.',
                  service: 'Calidad',
                  urlImage: 'assets/images/calidad.svg'
                ),
              ),
              Expanded(
                child: itempresentation(
                  description:
                      'No solo ofrecemos un producto, ofrecemos un compromiso. Nuestro equipo de soporte está listo para atender tus preguntas, resolver problemas y garantizar que obtengas el máximo valor de nuestra solución. Tu éxito es nuestro éxito.',
                  service: 'Soporte',
                  urlImage: 'assets/images/soporte.svg'
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget itempresentation({
    required String service,
    required String description,
    required String urlImage
  }) {
    return Column(
      children: [
        SizedBox(
          width: 150,
          height: 150,
          child: SvgPicture.asset(urlImage),
        ),
        Text(
          service,
          style: subtitlelora()
        ),
        const SizedBox(
          height: 8,
        ),
        SizedBox(
          width: 400,
          child: Text(
            description,
            textAlign: TextAlign.center,
            style: containtextlora(fontSize: 20)
          ),
        ),
      ],
    );
  }
}
