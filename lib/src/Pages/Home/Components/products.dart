import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:websiet_chapitec/src/Styles/styles.dart';
import 'package:url_launcher/url_launcher.dart';

class ProductsComponent extends StatefulWidget {
  const ProductsComponent({super.key});

  @override
  State<ProductsComponent> createState() => _ProductsComponentState();
}

class _ProductsComponentState extends State<ProductsComponent> {
  var isHovered = false;
  var isHovered_one = false;
  var isHovered_two = false;
  var isHovered_thre = false;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      color: Colors.grey.shade100,
      height: size.height * 0.6,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text('Productos',
              style: GoogleFonts.lora(
                  color: Colors.grey.shade700,
                  fontSize: 50,
                  fontWeight: FontWeight.w700)),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const SizedBox(width: 100),
              Flexible(
                child: productItem(
                  imagePath: 'assets/images/apps.jpg',
                  title: 'Marketing Digital',
                  nameService: 'Marketing Digital',
                  isHovered: isHovered,
                ),
              ),
              const SizedBox(width: 100),
              Flexible(
                child: productItem(
                  imagePath: 'assets/images/ecomerce.jpg',
                  title: 'Apps Moviles',
                  nameService: 'App Moviles',
                  isHovered: isHovered_one,
                ),
              ),
              const SizedBox(width: 100),
              Flexible(
                child: productItem(
                  imagePath: 'assets/images/mkt.jpg',
                  title: 'Desarrollo Web',
                  nameService: 'Desarrollo Web',
                  isHovered: isHovered_two,
                ),
              ),
              const SizedBox(width: 100),
              Flexible(
                child: productItem(
                  imagePath: 'assets/images/web.jpg',
                  title: 'Tiendas Online',
                  nameService: 'Tiendas Online',
                  isHovered: isHovered_thre,
                ),
              ),
              const SizedBox(width: 100),
            ],
          )
        ],
      ),
    );
  }

  Widget productItem({
    required String imagePath,
    required String title,
    required String nameService,
    required bool isHovered,
  }) {
    return StatefulBuilder(
      builder: (BuildContext context, void Function(void Function()) setState) {
        return MouseRegion(
          // cursor: SystemMouseCursors.click,
          onEnter: (event) {
            setState(() {
              isHovered = true;
            });
          },
          onExit: (event) {
            setState(() {
              isHovered = false;
            });
          },
          child: GestureDetector(
            onTap: () {
              openWhatsAppChat(nameService);
            },
            child: AnimatedContainer(
              transform: Matrix4.identity()
                ..translate(-0.1 * 100 / 2, -0.1 * 100 / 2)
                ..scale(isHovered ? 1.05 : 1.0)
                ..translate(0.1 * 100 / 2, 0.1 * 100 / 2),
              duration: const Duration(
                milliseconds: 500,
              ),
              curve: Curves.easeInOut,
              alignment: Alignment.center,
              child: Card(
                color: Colors.white,
                surfaceTintColor: Colors.white,
                elevation: isHovered ? 8 : 4,
                child: Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Column(
                    children: [
                      Image.asset(imagePath),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          title,
                          style: subtitlelora(),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  void openWhatsAppChat(String nameService) async {
    String url =
        "https://wa.me/51925576092/?text=Hola ChapItec quisiera mas informacion sobre el servicio de ${Uri.encodeComponent(nameService)}";

    // ignore: deprecated_member_use
    if (await canLaunch(url)) {
      // ignore: deprecated_member_use
      await launch(url);
    } else {
      throw 'No se pudo abrir WhatsApp.';
    }
  }
}
