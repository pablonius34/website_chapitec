import 'package:flutter/material.dart';

class TeamGroup extends StatelessWidget {
  const TeamGroup({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Reúnase con Nosotros',
            style: TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 10.0),
          Text(
            'Pequeña descripción del equipo aquí...',
            style: TextStyle(
              fontSize: 16.0,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _buildMemberCard(
                'assets/images/avatarHombre1.png',
                'Flavio Barnett',
                'Desarrollador Web',
                ['LinkedIn', 'Twitter'],
              ),
              _buildMemberCard(
                'assets/images/avatarMujer1.png',
                'Valeria Barquero ',
                'Diseñadora Grafica',
                ['Facebook', 'LinkedIn'],
              ),
              _buildMemberCard(
                'assets/images/avatarHombre2.png',
                'Pablo Damiano',
                'Desarrollador Movil',
                ['LinkedIn', 'Twitter'],
              ),
              _buildMemberCard(
                'assets/images/avatarMujer2.png',
                'Alondra Reyes',
                'Especialista en MD',
                ['Facebook', 'LinkedIn'],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildMemberCard(String imagePath, String name, String description,
      List<String> socialNetworks) {
    return Container(
      margin: EdgeInsets.all(10.0),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.grey,
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          CircleAvatar(
            radius: 50.0,
            backgroundImage: AssetImage(imagePath),
          ),
          SizedBox(height: 10.0),
          Text(
            name,
            style: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 5.0),
          Text(
            description,
            style: TextStyle(
              fontSize: 14.0,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 10.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: socialNetworks.map((social) {
              return Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Icon(
                  _getSocialIcon(social),
                  size: 20.0,
                ),
              );
            }).toList(),
          ),
        ],
      ),
    );
  }

  IconData _getSocialIcon(String social) {
    switch (social) {
      case 'Facebook':
        return Icons.facebook;
      case 'Twitter':
        return Icons.logo_dev_rounded;
      case 'Instagram':
        return Icons.logo_dev_rounded;
      case 'LinkedIn':
        return Icons.logo_dev_rounded;
      default:
        return Icons.error;
    }
  }
}
