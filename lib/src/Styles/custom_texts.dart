import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

var white_text = const TextStyle(color: Colors.white);

TextStyle subtitlelora({double? fontSize}) {
  return GoogleFonts.lora(
    color: Colors.grey,
    fontSize: fontSize ?? 20.0,
    fontWeight: FontWeight.bold,
  );
}

TextStyle titlelora({double? fontSize,Color? color}) {
  return GoogleFonts.lora(
    color:color??Colors.grey.shade700,
    fontSize: fontSize??50,
    fontWeight: FontWeight.w700,
  );
}

TextStyle containtextlora({double? fontSize}) {
  return GoogleFonts.lora(
    color: Colors.grey.shade700,
    fontSize: fontSize??15,
    fontWeight: FontWeight.w700,
  );
}
