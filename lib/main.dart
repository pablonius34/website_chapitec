import 'package:flutter/material.dart';
import 'package:websiet_chapitec/src/Pages/Home/home_screen.dart';
import 'package:websiet_chapitec/src/Routers/routers.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: Routers.routers,
      initialRoute: HomeScreen.name,
    );
  }
}